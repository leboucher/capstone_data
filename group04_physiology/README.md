This folder contains a tcx file which can be converted to CSV. It also contains
the same raw file in binary .fit format. If I don't convert the tcx file to CSV
in time, please let me know, or you can do it yourselves with TCXConverter.
