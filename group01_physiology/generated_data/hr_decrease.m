function res = hr_decrease(H0, H1, k01, t)

    res = H0 + exp(-k01.*t)*(H1 - H0);
end