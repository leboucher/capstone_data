%% Preamble and setup
maxMale = 169;
maxFem = 178;
thrMale = 143;
thrFem = 152;
restMale = 68;
restFem = 77;
aerMale = 120;
aerFem = 128;

k01Male = 0.019;
k01Fem = 0.023;

k12Male = 0.038;
k12Fem = 0.030;

k23Male = 0.022;
k23Fem = 0.028;

k34Male = 0.013;
k34Fem = 0.012;

k40Male = 1/90;
k40Fem = 1/76;



t = linspace(0, (3+6+3)*60, 12*60);

tWarmup = t(1:120);     % first 3 minutes warmup
tPhase12 = t(1:120);   % leave 6 minutes for this phase
tPhase23 = t(1:120);
tPhase34 = t(1:120);
tPhase40 = t(1:240);


k01MaleVec = norminv(rand(size(tWarmup)), k01Male, k01Male/20);
k01FemVec = norminv(rand(size(tWarmup)), k01Fem, k01Fem/22);
k12MaleVec = norminv(rand(size(tPhase12)), k12Male, k12Male/18);
k12FemVec = norminv(rand(size(tPhase12)), k12Fem, k12Fem/20);
k23MaleVec = norminv(rand(size(tPhase12)), k23Male, k23Male/18);
k23FemVec = norminv(rand(size(tPhase12)), k23Fem, k23Fem/20);
k34MaleVec = norminv(rand(size(tPhase12)), k34Male, k34Male/18);
k34FemVec = norminv(rand(size(tPhase12)), k34Fem, k34Fem/20);
k40MaleVec = norminv(rand(size(tPhase40)), k40Male, k40Male/22);
k40FemVec = norminv(rand(size(tPhase40)), k40Fem, k40Fem/18);

%% Generate data for the warmup for male/female on tread
warmupMale = hr_increase(restMale, aerMale, k01MaleVec, tWarmup);
warmupFem = hr_increase(restFem, aerFem, k01FemVec, tWarmup);

phase12Male = hr_increase(warmupMale(end), aerMale + 4, k12MaleVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 
phase12Fem = hr_increase(warmupFem(end), aerFem + 4, k12FemVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 

phase23Male = hr_increase(phase12Male(end), thrMale, k23MaleVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 
phase23Fem = hr_increase(phase12Fem(end), thrFem, k23FemVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 

phase34Male = hr_increase(phase23Male(end), maxMale, k34MaleVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 
phase34Fem = hr_increase(phase23Fem(end), maxFem, k34FemVec, tPhase12) ...
    + norminv(rand(size(tPhase12)), 0, 1.5); 


phase40Male = hr_decrease(restMale, phase34Male(end), k40MaleVec, tPhase40);
phase40Fem = hr_decrease(restFem, phase34Fem(end), k40FemVec, tPhase40);


plot(t, [warmupMale, phase12Male, phase23Male, phase34Male, phase40Male])
hold on
plot(t, [warmupFem, phase12Fem, phase23Fem, phase34Fem, phase40Fem]);

%% Write out to csv file
resultMale = [t; warmupMale, phase12Male, phase23Male, phase34Male, phase40Male]';
resultFem = [t; warmupFem, phase12Fem, phase23Fem, phase34Fem, phase40Fem]';

csvwrite('male_run1.csv', resultMale)
csvwrite('female_run1.csv', resultFem)