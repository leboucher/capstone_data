The first exercise was conducted on the treadmill.
 A 2 minute warm up time was allocated then the treadmill 
was set to a medium/high intensity for 6 minutes to gain 
steady state HR max. This was done for both male and female.


The second exercise followed this same procedure except on the bike. 
This was repeated for male and female.


The third procedure was on the bike with a 2 minute warm up. 
The intensity was then set to low for 2 minutes, 
medium for two minutes and then high for two minutes to 
test non-temporal parameters. 
This was repeated for male and female.
