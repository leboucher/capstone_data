Update for Garmin:
Files changed to TCX. If you order the filenames in each garmin folder by name, the top of the list are the oldest activities. I have named the tcx conversions as the same filename as the fit files. You will need to use TCX converter to change the files to csv.


The files in the current folder are from the Suunto watches. They are in XML format
and can be read by excel if imported properly.

In the garmin folders, the files are in the fit format. Please search for how to open
these kinds of files. You will have to download third party software. In the meantime,
I will look at sending you readable files, but let me know if you work it out on your own.


