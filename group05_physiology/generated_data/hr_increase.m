function res = hr_increase(H0, H1, k01, t)

    res = H1 - exp(-k01.*t)*(H1 - H0);
    %res = (H1 - H0)*exp(-k01*t) + H0;
end