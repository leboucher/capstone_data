%% Preamble and setup
clear all; clc; close all
% Set the time arrays for the phases of activity
tw = linspace(0,5*60, 5*60+1)';   % warmup time
tmax = linspace(0,80,81)';  % assume 4*20s intervals
tsubmax = linspace(0,120,121)';   % submax effort
tc = linspace(0,5*60,5*60+1)';  % cooldown time
ttmax = [tw; tw(end)+1+tmax; tw(end)+1+tmax(end)+1 + tc];  % total time
ttsubmax = [tw; tw(end)+1+tsubmax; tw(end)+2+tsubmax(end) + tc];


%% Set parameters and add noise for each subject
% par = [H0, Hw, Hm, Hsm20, Hsm40 Hsm60, Hsm80, Hc, 
%        kw, km, ksm20, ksm40, ksm60, ksm80, kc];

% par for max bike male = [74, 112, 191, 125, 140, 151, 170, 96, ...
%                 0.013, 0.042, 0.022, 0.026, 0.031, 0.036, 0.023];

m2f = (1.01 - 1.02)*rand(1,15) + 1.02;

bike2tread = ones(1,15);

randomise = (1.01 - 1.03)*rand(1,15) + 1.03;

par = [74, 112, 180, 125, 140, 151, 170, 96, ...
    0.013, 0.042, 0.022, 0.026, 0.031, 0.036, 0.023];

par = par.*randomise.*m2f.*bike2tread;

%% Randomise the parameters
N = 3;   % triplicate
noise = [0.2, 0.4];    % noise for strap vs optical
warmup = zeros(length(tw), N);
max_effort = zeros(length(tmax), N);
submax = zeros(length(tsubmax), N);
cooldown = zeros(length(tc), N);

for i = 1:N
    warmup(:,i) = hr_increase(par(1), par(2), ...
        norminv(rand(length(tw),1), par(9), par(9)*noise(1)), tw);
    
    submax(:,i) = hr_increase(warmup(end,i), par(4), ...
        norminv(rand(length(tsubmax),1),par(11), par(11)*noise(1)), tsubmax);
    
    cooldown(:,i) = hr_decrease(par(8), submax(end,i), ...
        norminv(rand(length(tc), 1), par(end), par(end)*noise(1)), tc);
    
end

submax_exercise20 = [warmup; submax; cooldown];
submax_exercise20 = [ttsubmax, submax_exercise20];


for i = 1:N
    warmup(:,i) = hr_increase(par(1), par(2), ...
        norminv(rand(length(tw),1), par(9), par(9)*noise(1)), tw);
    
    submax(:,i) = hr_increase(warmup(end,i),par(5),...
        norminv(rand(length(tsubmax),1),par(12), par(12)*noise(1)), tsubmax);
    
    cooldown(:,i) = hr_decrease(par(8), submax(end,i), ...
        norminv(rand(length(tc), 1), par(end), par(end)*noise(1)), tc);
    
end
submax_exercise40 = [warmup; submax; cooldown];
submax_exercise40 = [ttsubmax, submax_exercise40];

for i = 1:N
    warmup(:,i) = hr_increase(par(1), par(2), ...
        norminv(rand(length(tw),1), par(9), par(9)*noise(1)), tw);
    
    submax(:,i) = hr_increase(warmup(end,i),par(6),...
        norminv(rand(length(tsubmax),1),par(13), par(13)*noise(1)), tsubmax);
    
    cooldown(:,i) = hr_decrease(par(8), submax(end,i), ...
        norminv(rand(length(tc), 1), par(end), par(end)*noise(1)), tc);
    
end
submax_exercise60 = [warmup; submax; cooldown];
submax_exercise60 = [ttsubmax, submax_exercise60];

for i = 1:N
    warmup(:,i) = hr_increase(par(1), par(2), ...
        norminv(rand(length(tw),1), par(9), par(9)*noise(1)), tw);
    
    submax(:,i) = hr_increase(warmup(end,i),par(7),...
        norminv(rand(length(tsubmax),1),par(14), par(14)*noise(1)), tsubmax);
    
    cooldown(:,i) = hr_decrease(par(8), submax(end,i), ...
        norminv(rand(length(tc), 1), par(end), par(end)*noise(1)), tc);
    
end
submax_exercise80 = [warmup; submax; cooldown];
submax_exercise80 = [ttsubmax, submax_exercise80];

plot(submax_exercise60(:,1), submax_exercise60(:,2:end))